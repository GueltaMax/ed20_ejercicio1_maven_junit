package ed20_jUnit02_Ejercicio1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Ejercicio1TestJUnit {
	private Cadena target;

	@BeforeEach
	void setUp() throws Exception {
		target = new Cadena();
	}

	@Test
	void testLongitud() {
		int expected = 9;
		int expected2 = 0;
		int actual = target.longitud("holaclase");
		int actual2 = target.longitud("");
		assertEquals(expected, actual, "testing Longitud");
		assertEquals(expected2, actual2, "testing Longitud Empty");
	}
	@Test
	void testvocales() {
		int expected = 4;
		int expected2 = 0;
		int actual = target.vocales("holaclase");
		int actual2 = target.vocales("");
		assertEquals(expected, actual, "testing vocales");
		assertEquals(expected2, actual2, "testing vocales Empty");
	}
	@Test
	void testinvertir() {
		String expected = "esalcaloh";
		String expected2 = "";
		int actual = target.vocales("holaclase");
		int actual2 = target.vocales("");
		assertEquals(expected, actual, "testing invertir");
		assertEquals(expected2, actual2, "testing invertir Empty");
	}
	@Test
	void testcontarLetra() {
		int expected = 2;
		int expected2 = 0;
		int actual = target.contarLetra("holaclase", 'a');
		int actual2 = target.contarLetra("", 'a');
		assertEquals(expected, actual, "testing contarLetra");
		assertEquals(expected2, actual2, "testing contarLetra Empty");
	}
	
	

}
