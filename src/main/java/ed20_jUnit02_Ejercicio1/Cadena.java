package ed20_jUnit02_Ejercicio1;

public class Cadena {
	public int longitud(String cadena) {
		return cadena.length();
	}

	public int vocales(String cadena) {
		int count = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'o'
					|| cadena.charAt(i) == 'u') {
				count++;
			}
		}
		return count;
	}

	public String invertir(String cadena) {
		char ch[] = cadena.toCharArray();
		String inv = "";
		for (int i = ch.length - 1; i >= 0; i--) {
			inv += ch[i];
		}
		return inv;
	}

	public int contarLetra(String cadena, char caracter) {
		int count = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == caracter)
				count++;
		}

		return count;
	}
}
